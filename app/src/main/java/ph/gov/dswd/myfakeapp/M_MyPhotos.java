package ph.gov.dswd.myfakeapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import ph.gov.dswd.myfakeapp.adapters.ViewPagerAdapter;
import ph.gov.dswd.myfakeapp.object.Photo;

/**
 * Created by rhymartmanchus on 22/11/2017.
 */

public class M_MyPhotos extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.m_myphotos);

        final ViewPager viewPager = findViewById(R.id.vpLargePhoto);
        ImageView ivPhoto1 = findViewById(R.id.ivPhoto1);
        ImageView ivPhoto2 = findViewById(R.id.ivPhoto2);
        ImageView ivPhoto3 = findViewById(R.id.ivPhoto3);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        ArrayList<Photo> photos = new ArrayList<>();
        photos.add(new Photo("First Photo", "http://lorempixel.com/400/400/animals/1"));
        photos.add(new Photo("Second Photo", "http://lorempixel.com/400/400/animals/2"));
        photos.add(new Photo("Third Photo", "http://lorempixel.com/400/400/animals/3"));
        viewPagerAdapter.setPhotos(photos);

        viewPager.setAdapter(viewPagerAdapter);

        ivPhoto1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(0, true);
            }
        });

        ivPhoto2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(1, true);
            }
        });

        ivPhoto3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(2, true);
            }
        });
    }
}

package ph.gov.dswd.myfakeapp.object;

import android.util.Log;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by rhymartmanchus on 20/11/2017.
 */

// TABLE
@DatabaseTable
public class User {

    @DatabaseField(id = true)
    private int id;

    @DatabaseField
    private String name, username, email, phone;

    @ForeignCollectionField(columnName = "posts_fc", eager = true)
    private ForeignCollection<Post> posts;
    // My List of Posts ---

    @DatabaseField
    private String my_sample_column;

    public User() {
    }

    public String getMy_sample_column() {
        return my_sample_column;
    }

    public void setMy_sample_column(String my_sample_column) {
        this.my_sample_column = my_sample_column;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public ForeignCollection<Post> getPosts() {
        return posts;
    }

    public void setPosts(ForeignCollection<Post> posts) {
        this.posts = posts;
    }
}

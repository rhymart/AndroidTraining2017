package ph.gov.dswd.myfakeapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ph.gov.dswd.myfakeapp.OnChangeAddress;
import ph.gov.dswd.myfakeapp.R;

/**
 * Created by rhymartmanchus on 22/11/2017.
 */

public class NameFragment extends Fragment implements OnChangeAddress {

    public static NameFragment getInstance() { // GOOD PRACTICE
        NameFragment nameFragment = new NameFragment();
        return nameFragment;
    }

    public NameFragment() { }

    private String name, address;
    private TextView tvAddress;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_namedetails, null); // DOM Object

        TextView tvName = view.findViewById(R.id.tvName);
        tvAddress = view.findViewById(R.id.tvAddress);

        tvName.setText(name);
        tvAddress.setText(address);

        return view;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public void pleaseChangeAddres(String address) {
        tvAddress.setText(address);
    }
}

package ph.gov.dswd.myfakeapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.table.TableUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.List;

import ph.gov.dswd.myfakeapp.adapters.UsersAdapter;
import ph.gov.dswd.myfakeapp.object.User;

/**
 * Created by rhymartmanchus on 24/11/2017.
 */

public class M_UserActivity extends AppCompatActivity {

    private UsersAdapter usersAdapter;
    private RequestQueue requestQueue;

    private RecyclerView rvUsers;
    private TextView tvNoItems;

    private FakeDBHelper fakeDBHelper;

    public FakeDBHelper getHelper() {
        if(fakeDBHelper == null) {
            fakeDBHelper = OpenHelperManager.getHelper(this, FakeDBHelper.class);
        }
        return fakeDBHelper;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.m_useractivity);

        requestQueue = Volley.newRequestQueue(this);

        rvUsers = findViewById(R.id.rvUsers);
        tvNoItems = findViewById(R.id.tvNoItem);

        getSupportActionBar().setTitle("My Users");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvUsers.setLayoutManager(linearLayoutManager);
        rvUsers.setHasFixedSize(true);
        rvUsers.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        usersAdapter = new UsersAdapter(this);
        rvUsers.setAdapter(usersAdapter);

        requestForUsers();
    }

    private void requestForUsers() {
        JsonArrayRequest myUsers = new JsonArrayRequest("https://jsonplaceholder.typicode.com/users", new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                Gson gson = new Gson();
                for(int i = 0;i < response.length();i++) {
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);

                        User user = gson.fromJson(jsonObject.toString(), User.class);// User from JSON direct
                        // TODO Query user if it exists in the database
                        // SELECT * FROM users WHERE id = ?
                        User userDb = getHelper().getDao(User.class)
                                            .queryBuilder()
                                            .where()
                                            .eq("id", user.getId())
                                            .queryForFirst();

                        boolean isUserInDatabase = userDb != null; // TRUE when USER exists in the database
                        if(isUserInDatabase)
                            Log.e("User", "is in the database");
                        else
                            Log.e("User", "is NOT in the database");

                        // INSERT INTO
                        // UPDATE
                        getHelper().getDao(User.class).createOrUpdate(user);

                        usersAdapter.addUser(user);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                }

                if(usersAdapter.getItemCount() > 0) {
                    tvNoItems.setVisibility(View.GONE);
                    rvUsers.setVisibility(View.VISIBLE);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // What if no internet
                // TODO QUERY ALL FROM DATABASE
                // SELECT * FROM users

                Log.e("Volley", "onError");

                try {
                    List<User> userList = getHelper().getDao(User.class).queryForAll();

                    Log.e("Volley", "onError: "+userList.size());
                    for(User user : userList) { // FOREACH way
                        usersAdapter.addUser(user);
                    }

                    if(usersAdapter.getItemCount() > 0) {
                        tvNoItems.setVisibility(View.GONE);
                        rvUsers.setVisibility(View.VISIBLE);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
        myUsers.setShouldCache(false);
        requestQueue.add(myUsers);

//        getHelper().getDao(User.class).deleteBuilder().delete()// delete all values for User table
//        TableUtils.dropTable(getHelper().getDao(User.class), true); // Drop table
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        OpenHelperManager.releaseHelper();
    }
}

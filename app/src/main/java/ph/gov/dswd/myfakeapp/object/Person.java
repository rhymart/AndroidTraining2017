package ph.gov.dswd.myfakeapp.object;

import java.util.ArrayList;

/**
 * Created by rhymartmanchus on 20/11/2017.
 */

public class Person {

    private String name;
    private ArrayList<Address> addresses;

    public Person() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<Address> addresses) {
        this.addresses = addresses;
    }
}

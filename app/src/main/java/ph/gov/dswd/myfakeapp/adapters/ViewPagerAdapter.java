package ph.gov.dswd.myfakeapp.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import ph.gov.dswd.myfakeapp.fragments.PhotoFragment;
import ph.gov.dswd.myfakeapp.object.Photo;

/**
 * Created by rhymartmanchus on 22/11/2017.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Photo> photos = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setPhotos(ArrayList<Photo> photos) {
        this.photos = photos;
    }

    @Override
    public Fragment getItem(int position) {
        PhotoFragment photoFragment = PhotoFragment.getInstance();
        photoFragment.setUrl(photos.get(position).getUrl());
        photoFragment.setDescription(photos.get(position).getDescription());

        return photoFragment;
    }

    @Override
    public int getCount() {
        return photos.size();
    }
}

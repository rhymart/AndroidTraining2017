package ph.gov.dswd.myfakeapp.object;

import android.util.Log;

/**
 * Created by rhymartmanchus on 20/11/2017.
 */

@Deprecated
public class Dog extends Animal implements Abilities {

    @Override
    public String breed() {
        return "Chiwawa";
    }

    @Override
    public void eat() {
        Log.e("Dog eats", "dog food");
    }

    @Override
    public void travel() {

    }

    @Override
    public boolean doesBark() {
        return true;
    }

    @Override
    public boolean doesMeow() {
        return false;
    }

    public void myDogMethod() {

    }
}

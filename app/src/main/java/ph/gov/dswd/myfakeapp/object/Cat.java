package ph.gov.dswd.myfakeapp.object;

/**
 * Created by rhymartmanchus on 20/11/2017.
 */

@Deprecated
public class Cat extends Animal {

    private Dog dog;

    @Override
    public String breed() {

        dog.myDogMethod();
        return "Persian";
    }
}

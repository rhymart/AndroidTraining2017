package ph.gov.dswd.myfakeapp.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import ph.gov.dswd.myfakeapp.OnChangeAddress;
import ph.gov.dswd.myfakeapp.R;

/**
 * Created by rhymartmanchus on 22/11/2017.
 */

public class OtherFragment extends Fragment {

    public static OtherFragment getInstance() {
        OtherFragment otherFragment = new OtherFragment();
        return otherFragment;
    }

    private OnChangeAddress onChangeAddress;

    public void setOnChangeAddress(OnChangeAddress onChangeAddress) {
        this.onChangeAddress = onChangeAddress;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_otherlayout, null);

        Button btnShowADialog = view.findViewById(R.id.btnShowADialog);
        btnShowADialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder showAdialog = new AlertDialog.Builder(getActivity());
                showAdialog.setTitle("I'm a dialog!");
                showAdialog.setMessage("Change my address");
                showAdialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // CODE
                        if(onChangeAddress != null)
                            onChangeAddress.pleaseChangeAddres("Magarao, Camarines Sur");
                    }
                });
                showAdialog.setNegativeButton("Later", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // CODE
                    }
                });
                showAdialog.show();
            }
        });

        return view;
    }
}

package ph.gov.dswd.myfakeapp.object;

/**
 * Created by rhymartmanchus on 20/11/2017.
 */

public class Address {

    private String street, barangay, city;

    public Address() {
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBarangay() {
        return barangay;
    }

    public void setBarangay(String barangay) {
        this.barangay = barangay;
    }
}

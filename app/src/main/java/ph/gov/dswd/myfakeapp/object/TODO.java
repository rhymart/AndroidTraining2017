package ph.gov.dswd.myfakeapp.object;

/**
 * Created by rhymartmanchus on 20/11/2017.
 */

public class TODO {

    private int id;
    private String title, completed;

    public TODO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }
}

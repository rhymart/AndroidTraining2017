package ph.gov.dswd.myfakeapp.object;

/**
 * Created by rhymartmanchus on 20/11/2017.
 */

public class Photo {

    private int id;
    private String title, completed, description, url;

    public Photo() {
    }

    public Photo(String description, String url) {
        this.description = description;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }
}

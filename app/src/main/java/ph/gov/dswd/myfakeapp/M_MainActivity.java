package ph.gov.dswd.myfakeapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;

import ph.gov.dswd.myfakeapp.adapters.ModulesAdapter;

// Page
public class M_MainActivity extends AppCompatActivity implements ModulesAdapter.OnItemClickListener {

    private ModulesAdapter modulesAdapter; // our adapter

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.m_mainactivity); // Layout of the page

        RecyclerView rvModules = findViewById(R.id.rvModules);

        getSupportActionBar().setTitle("Modules");

        // To properly load the layout to the recyclerview as list
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvModules.setLayoutManager(linearLayoutManager);
        rvModules.setHasFixedSize(true);
        rvModules.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        // --- Dummy Data Only
        ArrayList<String> modules = new ArrayList<>();
        modules.add("Users");
        modules.add("Posts");
        // --- Dummy Data Only

        modulesAdapter = new ModulesAdapter(this, modules); // instance of the adapter
        modulesAdapter.setOnItemClickListener(this);
        rvModules.setAdapter(modulesAdapter); // Set the adapter to the recyclerview
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater menuInflater = getMenuInflater();
//        menuInflater.inflate(R.menu.m_mainactivity, menu);
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if(item.getItemId() == R.id.mAddPerson) {
//            // TODO Add a new name for the adapter, redirect to a new activity
//
//            Intent intent = new Intent(this, M_AddPersonForm.class);
//            // Pass a value to the add form activity
//            intent.putExtra("TITLE", "Add a person");
//            startActivityForResult(intent, 100);
//        }
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onClick(int position) {
        if(position ==  0) { // Redirect to UserActivity
            Intent intent = new Intent(this, M_UserActivity.class);
            startActivity(intent);
        }
        else if(position == 1) {
            Intent intent = new Intent(this, M_PostActivity.class);
            startActivity(intent);
        }
    }
}






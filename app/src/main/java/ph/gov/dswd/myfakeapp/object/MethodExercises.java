package ph.gov.dswd.myfakeapp.object;

/**
 * Created by rhymartmanchus on 20/11/2017.
 */

@Deprecated
public class MethodExercises {

    public static String screenName; // 3
    String screen_name;

    // 1
    public String myName() {
        return "Rhymart";
    }

    // 2
    private String myNickName(String nickname) {
        return nickname;
    }

    // 3
    public static String myScreenName() {
        return screenName;
    }

    // 4
    public MethodExercises(String nickname) {
        this.screen_name = nickname;
    }
}

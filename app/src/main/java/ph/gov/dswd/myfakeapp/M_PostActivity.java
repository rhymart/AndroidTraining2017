package ph.gov.dswd.myfakeapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ph.gov.dswd.myfakeapp.adapters.PostsAdapter;
import ph.gov.dswd.myfakeapp.object.Address;
import ph.gov.dswd.myfakeapp.object.Person;
import ph.gov.dswd.myfakeapp.object.Post;
import ph.gov.dswd.myfakeapp.object.User;

/**
 * Created by rhymartmanchus on 23/11/2017.
 */

public class M_PostActivity extends AppCompatActivity {

    private PostsAdapter postsAdapter;
    private TextView tvNoItem;
    private RecyclerView rvPosts;

    private RequestQueue requestQueue;

    private FakeDBHelper fakeDBHelper;

    public FakeDBHelper getHelper() {
        if(fakeDBHelper == null) {
            fakeDBHelper = OpenHelperManager.getHelper(this, FakeDBHelper.class);
        }
        return fakeDBHelper;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.m_postactivity);

        // This is required for the requests
        requestQueue = Volley.newRequestQueue(this);

        rvPosts = findViewById(R.id.rvPosts);
        tvNoItem = findViewById(R.id.tvNoItem);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvPosts.setLayoutManager(linearLayoutManager);
        rvPosts.setHasFixedSize(true);
        rvPosts.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        postsAdapter = new PostsAdapter(this);
        postsAdapter.setOnClickPost(new PostsAdapter.OnClickPost() {
            @Override
            public void onClick(Post post) {
                Toast.makeText(M_PostActivity.this, post.getUser().getName(), Toast.LENGTH_LONG).show();
            }
        });
        rvPosts.setAdapter(postsAdapter);

        requestForThePosts();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        OpenHelperManager.releaseHelper();
    }

    private void requestForThePosts() {
        // TODO, Let's try to fetch the posts from our FAKE API
        // ...after that load to the recyclerview

        Log.e("Starting request", "now..");
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("https://jsonplaceholder.typicode.com/posts",
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e("request", "DONE..");
                        Log.e("Response", response.toString());

                        Gson gson = new Gson();
                        for(int i = 0;i < response.length();i++) {
                            try {
                                JSONObject jsonObject = response.getJSONObject(i); // Get the POST object for saving
                                Post post = gson.fromJson(jsonObject.toString(), Post.class);
                                // SELECT * FROM users WHERE id = ?
                                User user = getHelper().getDao(User.class).queryBuilder().where()
                                        .eq("id", post.getUserId()).queryForFirst();
                                post.setUser(user); // Assign the USER from DB to the POST

                                getHelper().getDao(Post.class).createOrUpdate(post);

                                // TODO Save to the database
                                postsAdapter.addItem(post);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }

                        if(postsAdapter.getItemCount() > 0) {
                            tvNoItem.setVisibility(View.GONE);
                            rvPosts.setVisibility(View.VISIBLE);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle when error
                        // No internet
                        // 400
                        // 502

                        // TODO Handle when internet is not present of any error, load from the database
                        try {
                            List<Post> postList = getHelper().getDao(Post.class).queryForAll();

                            for(Post post : postList)
                                postsAdapter.addItem(post);

                            if(postsAdapter.getItemCount() > 0) {
                                tvNoItem.setVisibility(View.GONE);
                                rvPosts.setVisibility(View.VISIBLE);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                    }
                });
        jsonArrayRequest.setShouldCache(false);
        requestQueue.add(jsonArrayRequest);
    }

}

package ph.gov.dswd.myfakeapp.object;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rhymartmanchus on 20/11/2017.
 */

@DatabaseTable
public class Post {

    @DatabaseField(id = true)
    private int id;
    @DatabaseField
    private String title;
    @DatabaseField
    private String body;
    @DatabaseField(foreign = true, columnName = "user_id", foreignAutoRefresh = true)
    private User user;

    private int userId;

    public Post() {
    }

    public Post(String title, String body) {
        this.title = title;
        this.body = body;
    }

    public Post(User user) {
        this.user = user;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

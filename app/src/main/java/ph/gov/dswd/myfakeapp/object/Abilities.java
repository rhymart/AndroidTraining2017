package ph.gov.dswd.myfakeapp.object;

/**
 * Created by rhymartmanchus on 20/11/2017.
 */
@Deprecated
public interface Abilities {
    void eat();
    void travel();
    boolean doesBark();
    boolean doesMeow();
}

package ph.gov.dswd.myfakeapp.object;

/**
 * Created by rhymartmanchus on 21/11/2017.
 */

public class Name {

    private String name, address, url;

    public Name(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public Name(String name, String address, String url) {
        this.name = name;
        this.address = address;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String displayDetails() {
        return name+" | "+address;
    }
}

package ph.gov.dswd.myfakeapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ph.gov.dswd.myfakeapp.R;
import ph.gov.dswd.myfakeapp.object.Post;

/**
 * Created by rhymartmanchus on 23/11/2017.
 */

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder> {

    public interface OnClickPost {
        void onClick(Post post);
    }

    private OnClickPost onClickPost;
    private Context context;
    private ArrayList<Post> posts = new ArrayList<>();

    public PostsAdapter(Context context) {
        this.context = context;
    }

    public PostsAdapter(Context context, ArrayList<Post> posts) {
        this.context = context;
        this.posts = posts;
    }

    public void setOnClickPost(OnClickPost onClickPost) {
        this.onClickPost = onClickPost;
    }

    public void addItem(Post post) {
        this.posts.add(post);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.listitem_post, parent, false); // CHANGED TO MAKE CARD
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvTitle.setText(posts.get(position).getTitle());
        holder.tvBody.setText(posts.get(position).getBody());
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTitle, tvBody;

        public ViewHolder(View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvBody = itemView.findViewById(R.id.tvBody);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(onClickPost != null)
                onClickPost.onClick(posts.get(getLayoutPosition()));
        }
    }

}

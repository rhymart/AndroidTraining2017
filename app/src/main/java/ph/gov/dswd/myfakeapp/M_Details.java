package ph.gov.dswd.myfakeapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import ph.gov.dswd.myfakeapp.fragments.NameFragment;
import ph.gov.dswd.myfakeapp.fragments.OtherFragment;

/**
 * Created by rhymartmanchus on 22/11/2017.
 */

public class M_Details extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.m_details);

        getSupportActionBar().setTitle("My Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NameFragment nameFragment1 = NameFragment.getInstance();
        nameFragment1.setName(getIntent().getStringExtra("NAME"));
        nameFragment1.setAddress(getIntent().getStringExtra("ADDRESS"));

//        OtherFragment otherFragment = new OtherFragment();
//        otherFragment.setOnChangeAddress(nameFragment1);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.flContainer1, nameFragment1)
//                .add(R.id.flContainer2, otherFragment)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}

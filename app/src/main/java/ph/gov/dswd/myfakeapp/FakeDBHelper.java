package ph.gov.dswd.myfakeapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.io.File;
import java.sql.SQLException;
import java.util.Calendar;

import ph.gov.dswd.myfakeapp.object.Post;
import ph.gov.dswd.myfakeapp.object.User;

/**
 * Created by rhymartmanchus on 24/11/2017.
 */
// DATABASE HELPER
public class FakeDBHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "fakedb.db";
    private static final int DATABASE_VERSION = 2;

    public FakeDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        // Creates the database
        // Creates all the tables
        try {
            TableUtils.createTable(connectionSource, User.class); // Create user table
            TableUtils.createTable(connectionSource, Post.class); // Create user table
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        // Migration queries
        // e.g. Add columns, tables...

        int currentVersion = oldVersion+1; // 1
        while (currentVersion <= newVersion) { // newversion = 10
            switch (currentVersion) {
                case 2: {
                    // Execute
                    try {
                        TableUtils.createTable(connectionSource, Post.class); // Create user table
                        getDao(User.class)
                                .executeRaw("ALTER TABLE `user` ADD COLUMN my_sample_column TEXT"); // ---
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                } break;
                // and other migration codes
            }

            currentVersion++;
        }

    }
}

package ph.gov.dswd.myfakeapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by rhymartmanchus on 21/11/2017.
 */

public class M_AddPersonForm extends AppCompatActivity {

    private EditText etName, etAddress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.m_addpersonform);

        etName = findViewById(R.id.etName);
        etAddress = findViewById(R.id.etAddress);
        Button btnAdd = findViewById(R.id.btnAdd);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = etName.getText().toString();
                // TODO Pass the address value

                Intent intent = new Intent();
                intent.putExtra("NAME", name);

                // Pass data to the previous activity
                setResult(200, intent);
                finish();
            }
        });

        // get the value from the intent, then load as a title to the actionbar
        getSupportActionBar().setTitle(getIntent().getStringExtra("TITLE"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // to show the back arrow in the actionbar

        // Put a back button to the actionbar
        // Change the actionbar title
        // Create a listener for btnAdd
        // Pass the etName and etAddress values back to the M_MainActivity
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) // ID of the back arrow in the actionbar
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        setResult(400);
        super.onBackPressed();
    }
}

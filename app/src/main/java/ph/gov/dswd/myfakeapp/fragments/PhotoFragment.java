package ph.gov.dswd.myfakeapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import ph.gov.dswd.myfakeapp.R;

/**
 * Created by rhymartmanchus on 22/11/2017.
 */

public class PhotoFragment extends Fragment {

    private String url, description;

    public static PhotoFragment getInstance() {
        PhotoFragment photoFragment = new PhotoFragment();
        return photoFragment;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo, null);

        ImageView ivPhoto = view.findViewById(R.id.ivPhoto);
        TextView tvDescription = view.findViewById(R.id.tvDescription);

        Picasso.with(getContext()).load(url).into(ivPhoto);
        tvDescription.setText(description);

        return view;
    }
}

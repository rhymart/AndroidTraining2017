package ph.gov.dswd.myfakeapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 *
 * tv - TextView
 * iv - ImageView
 * rv - RecyclerView
 * btn - Button
 * ll - LinearLayout
 * et - EditText
 *
 * Created by rhymartmanchus on 21/11/2017.
 */
                                    // Step 1               Step 4 -- set ViewHolder
public class ModulesAdapter extends RecyclerView.Adapter<ModulesAdapter.ViewHolder> { // Step 5 --- Implement methods

    public interface OnItemClickListener {
        void onClick(int position);
    }

    private Context context; // from the activity
    private ArrayList<String> names = new ArrayList<>(); // ** Data container **
    private OnItemClickListener onItemClickListener;

    public ModulesAdapter(Context context) {
        this.context = context;
    }

    public ModulesAdapter(Context context, ArrayList<String> names) {
        this.context = context;
        this.names = names;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override // Loading of the layout to this adapter
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) { // Get an instance of views from the layout
        View view = LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_1, parent, false); // CHANGED TO MAKE CARD
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override // Loading of data to the view
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvName.setText(names.get(position)); // Load data of name to the textview tvName
//        holder.tvAddress.setText(names.get(position).getAddress());

//        // Load image
//        Picasso.with(context)
//                .load(names.get(position).getUrl())
//                .into(holder.ivPhoto);
    }

    @Override // Count of data to be shown in the list
    public int getItemCount() {
        return names.size();
    }
//
//    // Adding of a new name in your dataset
//    public void addName(Name name) {
//        this.names.add(0, name);
//        notifyDataSetChanged();
//    }

    // Step 2: Holds the view instances
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // Views from the layout
        TextView tvName;
//        , tvAddress;
//        ImageView ivPhoto;

        // Step 3
        public ViewHolder(View itemView) {
            super(itemView);

            tvName = itemView.findViewById(android.R.id.text1); // Attaching of views from the layout to a variable via itemView
//            tvAddress = itemView.findViewById(R.id.tvAddress);
//            ivPhoto = itemView.findViewById(R.id.ivPhoto);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            if(onItemClickListener != null)
                onItemClickListener.onClick(getLayoutPosition());
        }
    }

}
